import React from 'react';

import {
  ChakraProvider
} from '@chakra-ui/react';

import theme from './utils/theme/theme.util';

import { BrowserRouter, Route, Routes } from "react-router-dom"

import NavigationBar from "./routes/NavigationBar/NavigationBar.component"
import SignInPage from "./routes/SignInPage/SignInPage.component"
import ChatPage from "./routes/ChatPage/ChatPage.component"
import {UserProvider} from "./context/UserContext/UserContext.component"
import {ChatProvider} from "./context/ChatContext/ChatContext.component"

// 2. Call `extendTheme` and pass your custom values


function App() {
  return (
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <UserProvider>
          <ChatProvider>
          <Routes>
          <Route path="/" element={<NavigationBar/>}>
            <Route index element={<ChatPage/>}/>
            <Route path="/sign-in" element={<SignInPage/>} />
          </Route>
          </Routes>
          </ChatProvider>
        </UserProvider>
      </BrowserRouter>
    </ChakraProvider>
  );
}

export default App;
