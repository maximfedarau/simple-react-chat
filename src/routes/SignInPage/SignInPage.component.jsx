import { Center, Button, Text, Box } from "@chakra-ui/react"
import AlreadySignedInPanel from "../../components/AlreadySignedInPanel/AlreadySignedInPanel.component"

import React from "react"

import {FaGoogle, FaGithub} from "react-icons/fa"

import {signInWithGoogle,signInWithGitHub} from "../../utils/firebase/firebase.utils"

import { UserContext } from "../../context/UserContext/UserContext.component"
import SignInPanel from "../../components/SignInPanel/SignInPanel.component"

export default function SignInPage() {

    const {user} = React.useContext(UserContext)

    async function onClickGoogleHandler() {
        await signInWithGoogle()
    }

    async function onClickGitHubHandler() {
        await signInWithGitHub()
    }

    return (
        <Center marginTop="10px">
            {(Object.keys(user).length) ? 
            <AlreadySignedInPanel displayName={user.displayName}/>
             : 
            <SignInPanel googleButtonProps={
                {
                    bg: "red.500",
                    color: "white",
                    variant: "solid",
                    _hover: {bg: "red.400"},
                    leftIcon: <FaGoogle/>,
                    onClick: onClickGoogleHandler
                }
            } githubButtonProps={
                {   
                    bg: 'gray.900',
                    _hover: {bg: 'gray.600'},
                    color: 'white',
                    leftIcon: <FaGithub/>,
                    onClick: onClickGitHubHandler
                }
            }/> }
        </Center>
    )
}