import { Button, Divider, Flex, HStack } from "@chakra-ui/react"
import {IoIosHome, IoMdPerson, IoMdExit} from "react-icons/io"

import { UserContext } from "../../context/UserContext/UserContext.component"

import { Link, Outlet } from "react-router-dom"

import { signOutAuth } from "../../utils/firebase/firebase.utils"

import React from "react"

export default function NavigationBar() {

    const {user} = React.useContext(UserContext)

    async function onClickSignOutHandler() {
        await signOutAuth()
    }
    return (
        <div>
            <Flex 
            w="100%"
            px="6"
            py="5"
            align="center"
            justify="space-between"
            bg="#a2c0cc"
            >
                <div></div>
                <HStack gap={10}>
                    <Link to="/">
                        <Button>
                            <IoIosHome/>
                        </Button>
                    </Link>&nbsp;
                    <Link to="/sign-in">
                        <Button>
                            <IoMdPerson/>
                        </Button>
                    </Link>
                </HStack>
                <HStack>
                    {Object.keys(user).length && <Button onClick={onClickSignOutHandler}><IoMdExit/></Button>}
                </HStack>
            </Flex>
            <Divider/>
            <Outlet/>
        </div>
    )
}