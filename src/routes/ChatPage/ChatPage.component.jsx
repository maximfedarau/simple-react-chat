import { Center, Input, Button, Box } from "@chakra-ui/react"

import { IoMdArrowRoundForward } from "react-icons/io"

import {
     createNewMessage
} from "../../utils/firebase/firebase.utils"

import React from "react"

import { UserContext } from "../../context/UserContext/UserContext.component"
import { ChatContext } from "../../context/ChatContext/ChatContext.component"
import MessageCard from "../../components/MessageCard/MessageCard.component"

export default function ChatPage() {

    const [messageText,setMessageText] = React.useState('')

    const {user} = React.useContext(UserContext)
    const {displayName} = user

    const {chatData} = React.useContext(ChatContext)

    function onChangeHandler(event) {
        setMessageText(event.target.value)
    }

    async function onClickMessageHandler() {
        await createNewMessage(messageText,displayName)
        setMessageText('')
    }

    return (
        <Center onKeyPress={(event) => {
            if (event.charCode === 13) onClickMessageHandler()
        }}>
            <Box>
            <Box bg={"white"} maxHeight="400px" overflowY={"scroll"} overflowX="scroll" w="600px" marginTop={"20px"} borderRadius="1rem">
            {Object.keys(chatData).sort().map((value,key) => {
                return <MessageCard key={key} username={chatData[value].username} content={chatData[value].content}/>
            })}</Box>
            {(Object.keys(user).length) ? <Box>
            <Input bg="white" w="400px" placeholder="Your message:" value={messageText} onChange={onChangeHandler} marginTop="20px"/>
            <Button onClick={onClickMessageHandler} marginLeft="20px"><IoMdArrowRoundForward/></Button>
            </Box> : <Box>You are not Signed In.</Box>}
            </Box>
        </Center>
    )
}