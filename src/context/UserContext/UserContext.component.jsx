import React from "react"

import { 
    onAuthStateChangedListener
 } from "../../utils/firebase/firebase.utils"

export const UserContext = React.createContext({
    user: {},
    setUser: () => {}
})

export function UserProvider({children}) {
    const [user,setUser] = React.useState({})

    React.useEffect(async () => {
        const subscribe = await onAuthStateChangedListener((user) => {
            if (user) {
                setUser(user)
            } else {
                setUser({})
            }
        })

        return subscribe
    },[])

    const value = {user}

    return (
        <UserContext.Provider value={value}>
            {children}
        </UserContext.Provider>
    )
}