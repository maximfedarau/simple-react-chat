import React from "react"

import { 
    onSnapshotListener
 } from "../../utils/firebase/firebase.utils"

export const ChatContext = React.createContext({
    chatData: {},
    setChatData: () => {}
})

export function ChatProvider({children}) {
    const [chatData, setChatData] = React.useState({})

    const value = {chatData}

    React.useEffect(async () => {
        const subscribe = await onSnapshotListener((doc) => {
            setChatData(doc.data())
        })
        return subscribe
    },[])

    return (
        <ChatContext.Provider value={value}>
            {children}
        </ChatContext.Provider>
    )
}