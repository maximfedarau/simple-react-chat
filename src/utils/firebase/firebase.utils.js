import { initializeApp } from "firebase/app";

import {
  getAuth,
  GoogleAuthProvider,
  GithubAuthProvider,
  signInWithPopup,
  onAuthStateChanged,
  signOut
} from 'firebase/auth'

import {
  getFirestore,
  doc,
  getDoc,
  setDoc,
  onSnapshot
} from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyDkDt0I_sCmXyEjdxyu31pFkA0G7zE7BC4",
  authDomain: "simple-chat-react-app.firebaseapp.com",
  projectId: "simple-chat-react-app",
  storageBucket: "simple-chat-react-app.appspot.com",
  messagingSenderId: "225821540855",
  appId: "1:225821540855:web:f7501166117bc58ac76ef2"
};

const app = initializeApp(firebaseConfig);

const auth = getAuth()

const googleProvider = new GoogleAuthProvider()
googleProvider.setCustomParameters({
  prompt: 'select_account'
})

export async function signInWithGoogle() {
  return await signInWithPopup(auth,googleProvider)
}

const githubProvider = new GithubAuthProvider()
githubProvider.setCustomParameters({
  prompt: 'select_account'
})

export async function signInWithGitHub() {
  return await signInWithPopup(auth,githubProvider)
}

export async function onAuthStateChangedListener(callback) {
  await onAuthStateChanged(auth,callback)
}

export async function signOutAuth() {
  return await signOut(auth)
}

const db = getFirestore()

export async function createNewMessage(content,username) {
  const userDocRef = doc(db,'chat','messages')

  const messageTime = new Date().getTime()
  await setDoc(userDocRef,{
      [messageTime]: {
        "username": username,
        "content": content
      }
  },{merge:true})

  return userDocRef
}

export async function onSnapshotListener(callback) {
  const userDocRef = doc(db,'chat','messages')
  return await onSnapshot(userDocRef,callback)
}