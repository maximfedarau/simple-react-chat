import {
    extendTheme
} from "@chakra-ui/react"

const theme = extendTheme({
    styles: {
      global: {
        body: {
          bg: '#fed9b4'
        }
      }
    }
})

export default theme