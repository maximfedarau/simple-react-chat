import { Box, Text, Button } from "@chakra-ui/react"

import React from "react"


export default function SignInPanel({googleButtonProps,githubButtonProps}) {

    return (
        <Box>
            <Text textAlign="center" fontSize={35}>Sign In</Text>
            <Button {...googleButtonProps}>Sign In with Google</Button><br/><br/>
            <Button {...githubButtonProps}>Sign In with GitHub</Button>
        </Box>
    )
}