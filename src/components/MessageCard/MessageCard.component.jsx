import { Box, Divider, Text } from '@chakra-ui/react'
import React from 'react'

export default function MessageCard({username,content}) {
    return (
        <Box padding={"10px"}>
            <Text color="blue.500">{username}:</Text>
            <h1>{content}</h1>
            <Divider/>
        </Box>
    )
}