import { Box, Text } from "@chakra-ui/react"

import React from "react"

export default function AlreadySignedInPanel({displayName}) {
    return (
        <Box>
            <Text textAlign="center" fontSize={35}>You are already Signed In.</Text>
            <Text textAlign="center" fontSize={20}>Your public name: {displayName}</Text>
        </Box>
    )
}